aspect Inline {
	syn boolean Block.shouldBeInlined()
		= getModifiers().isInline() && canBeInlined();
	eq RedeclareBlock.shouldBeInlined() {
		boolean inline = getModifiers().isInline();
		if (redeclaredBlock() != null) {
			inline = inline || redeclaredBlock().shouldBeInlined();
		}
		return inline && canBeInlined();
	}
	syn boolean Block.canBeInlined()
		= !isOnTypeCycle() && type().isDiagramType()
			&& (type().containsBlocks() || type().containsVariables());

	syn boolean Node.isInlined();
	eq Parameter.isInlined() = false;
	eq WiringParameter.isInlined() = false;
	eq Variable.isInlined() = false;
	eq Block.isInlined() = false;
	eq Literal.isInlined() = false;
	eq InlinedBlock.isInlined() = true;
	eq InlinedVariable.isInlined() = true;

	syn Block Node.inlinedBlock();
	eq Parameter.inlinedBlock() = null;
	eq WiringParameter.inlinedBlock() = null;
	eq Variable.inlinedBlock() = null;
	eq InlinedVariable.inlinedBlock() = getInlinedBlock();
	eq Literal.inlinedBlock() = null;
	eq Block.inlinedBlock() = null;
	eq InlinedBlock.inlinedBlock()
		= getInlinedBlocks().get(0);
	syn java.util.List<Block> Block.inlinedBlocks()
		= Collections.emptyList();
	eq InlinedBlock.inlinedBlocks() = getInlinedBlocks();

	syn DiagramType Block.lastDiagramType() = diagramType();
	eq InlinedBlock.lastDiagramType() = getLastDiagramType();
	
	syn boolean DiagramType.isEnclosedBy(DiagramType dt)
		= dt == this || isEnclosedByInh(dt);
	syn boolean Block.isEnclosedBy(DiagramType dt) = isEnclosedByInh(dt);
	inh boolean DiagramType.isEnclosedByInh(DiagramType dt);
	inh boolean Block.isEnclosedByInh(DiagramType dt);
	eq DiagramType.getChild().isEnclosedByInh(DiagramType dt)
		= dt == this || isEnclosedByInh(dt);
	eq Program.getChild().isEnclosedByInh(DiagramType dt) = false;
	
	syn boolean Block.isEnclosedBy(Block b)
		= b == this || isEnclosedByInh(b);
	inh boolean Block.isEnclosedByInh(Block b);
	eq Block.getType().isEnclosedByInh(Block b)
		= b == this || isEnclosedByInh(b);
	eq Program.getChild().isEnclosedByInh(Block b) = false;

	syn boolean TypeDecl.containsBlocks() = false;
	eq DiagramType.containsBlocks() = blocks().getNumChild() > 0;

	syn boolean TypeDecl.containsVariables() = false;
	eq DiagramType.containsVariables() = variables().getNumChild() > 0;

	syn boolean Connection.isInlined() = false;
	eq InlinedConnection.isInlined() = true;
	
	inh boolean Anchor.belongsToInlinedBlock();
	eq InlinedBlock.getChild().belongsToInlinedBlock() = true;
	eq Program.getChild().belongsToInlinedBlock() = false;
	
	eq InlinedName.flatName() = getOuter().flatName() + INLINE_SEP + getInner().flatName();
	eq InlinedName.name() = getOuter().name() + INLINE_SEP + getInner().name();
	
	syn String Node.inlinedName() = name().substring(name().lastIndexOf(ASTNode.INLINE_SEP)+1);
}

aspect InlineErrors {
	Block contributes error("Only blocks of diagram types can be inlined")
		when getModifiers().isInline() && !type().isUnknown() && !type().isDiagramType()
		to CompilationUnit.errors();
}