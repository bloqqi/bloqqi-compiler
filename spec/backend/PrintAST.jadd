/** For debugging purposes */
aspect PrintAST {
	syn String ASTNode.printASTInfo() = "";
	eq VarUse.printASTInfo() = decl() + ", " + type();
	eq AbstractSimpleVarUse.printASTInfo() = getID() + ", " + decl() + ", " + type();
	eq Block.printASTInfo() {
		String s = name() + ", ";
		s += diagramType().name() + ", ";
		s += declaredInDiagramType().name() + ", ";
		s += lastDiagramType().name();
		return s;
	}
	eq ConfBlock.printASTInfo() {
		String s = name() + ", " + type();
		if (type().isDiagramType()) {
			DiagramType dt = ((DiagramType) type());
			s += ", in=" + dt.allInputVariables();
			s += ", out=" + dt.allOutputVariables();
		}
		return s;
	}
	eq Port.printASTInfo() = "in=" + ingoingConnections() + ", out=" + outgoingConnections();
	eq DotVarUse.printASTInfo() = toString() + ", " + decl();
	eq Parameter.printASTInfo() = uses().toString();
	eq DiagramType.printASTInfo() = name() + ", " + sortedRecommendations();
	eq RecommendationBlock.printASTInfo() = interceptingAnchors() + ", " + conflictingRecommendations().toString();
	eq TypeUse.printASTInfo() = toString() + ", " + decl();
	eq Variable.printASTInfo() = name();
	eq ConfUse.printASTInfo() = toString() + ", " + blockDecl() + ", " + varPortDecl();
	eq VarPort.printASTInfo() = name() + ", in=" + incoming() + ", out=" + outgoing();
	eq ConfConnection.printASTInfo() = toString();
	eq FeatureIdUse.printASTInfo() = "" + decl();
	eq FeatureDeclStmt.printASTInfo() = "excludes: " + excludesFeaturesTransitive();

	//eq ConfDotUse.printASTInfo() = "" + decl();

	public void ASTNode.printAST() {
		printAST("", 0);
	}
	protected void ASTNode.printAST(String prefix, int i) {
		i = printASTprintName(prefix, i);
		for (ASTNode child: astChildren()) {
			child.printAST("", i);
		}
	}
	protected void Program.printAST(String prefix, int i) {
		i = printASTprintName(prefix, i);
		for (CompilationUnit cu: getCompilationUnits()) {
			cu.printAST("", i);
		}
	}
	protected int ASTNode.printASTprintName(String prefix, int i) {
		String name = getClass().getSimpleName();
		if (!name.equals("AST.Opt")) {
			name = name.substring(name.indexOf('.')+1);
			name = prefix.isEmpty() ? name : prefix + ":" + name;
			String info = printASTInfo();
			name += info.isEmpty() ? "" : "(" + info + ")";
			System.out.println(getIndent(i) + name);
			i++;
		}
		return i;
	}
	protected void DiagramType.printAST(String prefix, int i) {
		super.printAST(prefix, i);
		getInParameters().printAST("InParameters", i+1);
		getOutParameters().printAST("OutParameters", i+1);
		getVariables().printAST("Variables", i+1);
		getBlocks().printAST("Blocks", i+1);
		getConnections().printAST("Connections", i+1);
		recommendedElements().printAST("recommendedElements()", i+1);
		System.out.println(getIndent(i) + "featureStmts()");
		for (FeatureStmt stmt: featureStmts()) {
			stmt.printAST("", i+2);
		}
	}
	protected void Block.printAST(String prefix, int i) {
		super.printAST(prefix, i);
		getInPorts().printAST("InPorts", i+1);
		getOutPorts().printAST("OutPorts", i+1);
	}
	protected void WiredBlock.printAST(String prefix, int i) {
		super.printAST(prefix, i);
		flowDecls().printAST("flowDecls", i+1);
	}
	/* Help method for indenting */
	public String ASTNode.getIndent(int t) {
		StringBuilder sb = new StringBuilder(t);
		for (int i = 0; i < t; i++) {
			sb.append("  ");
		}
		return sb.toString();
	}

	protected void ConfBlock.printAST(String prefix, int i) {
		super.printAST(prefix, i);
		getInVarPorts().printAST("InVarPorts", i+1);
		getOutVarPorts().printAST("OutVarPorts", i+1);
	}
}
