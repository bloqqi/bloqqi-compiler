aspect IsParameterExposed {
	/**
	 * Check if inner parameter (possible nested) is exposed as an outer parameter
	 */
	syn Parameter DiagramType.isInnerParameterExposed(String path) {
		VarDecl vdPar = lookupInnerVarDecl(path);
		if (!path.contains(".")) {
			return vdPar != null && vdPar instanceof Parameter
					? (Parameter) vdPar
					: null;
		}
		
		String pathBlocks = path.substring(0, path.lastIndexOf("."));
		VarDecl vdInnerComp = lookupInnerVarDecl(pathBlocks);
		
		if (!(vdPar instanceof Parameter) || !(vdInnerComp instanceof Block)) {
			return null;
		}
		
		Parameter par = (Parameter) vdPar;
		Block innerComp = (Block) vdInnerComp;
		if (!innerComp.isEnclosedBy(this)) {
			return null;
		}
		
		return isInnerParameterExposed(innerComp, par);
	}
	
	/**
	 * Helper attributes
	 */
	syn Parameter DiagramType.isInnerParameterExposed(Block comp, Parameter par) {
		if (this == par.diagramType()) {
			return par;
		}
		
		Port cp = comp.findPort(par);
		if (cp == null) {
			return null;
		}
		
		if (par.isInParameter() && !cp.ingoingConnections().isEmpty()) {
			Connection conn = cp.ingoingConnections().iterator().next();
			if (conn.getSource().node().isInParameter()) {
				Parameter p = (Parameter) conn.getSource().node();
				return isInnerParameterExposed(comp.enclosingBlock(), p.declaredParameter());
			}
		} else if (!par.isInParameter() && !cp.outgoingConnections().isEmpty()){
			Connection conn = cp.outgoingConnections().iterator().next();
			if (conn.getTarget().node().isOutParameter()) {
				Parameter p = (Parameter) conn.getTarget().node();
				return isInnerParameterExposed(comp.enclosingBlock(), p.declaredParameter());
			}
		}
		
		return null;
	}

	syn Port Block.findPort(Parameter p) {
		VarDecl vd = findMember(p.name());
		if (vd instanceof Port) {
			return (Port) vd;
		} else {
			return null;
		}
	}
}

/**
 * Checks that a subtype can be extracted as a block
 */
aspect CanExtractSubType {
	syn boolean DiagramType.isLocallyEmpty()
		= getNumLocalInParameter() == 0
			&& getNumLocalOutParameter() == 0
			&& getNumLocalVariable() == 0
			&& getNumLocalBlock() == 0
			&& getNumFlowDecl() == 0;
	
	syn boolean DiagramType.canExtractSubtypeAsBlock() {
		for (Block b: blocks()) {
			if (b.isInherited()
					&& b.isReachedFromLocalNode()
					&& b.isReachingLocalNode()) {
				return false;
			}
		}
		return true;
	}
	
	syn boolean Node.isReachedFromLocalNode() circular[true] {
		if (isInherited()) {
			for (Node n: pred()) {
				if (n.isReachedFromLocalNode()) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}
	
	syn boolean Node.isReachingLocalNode() circular[true] {
		if (isInherited()) {
			for (Node n: succ()) {
				if (n.isReachingLocalNode()) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}
}

aspect ExtractSubType {
	public Pair<DiagramType, WiredBlock> DiagramType.extractsubTypeAsWiredBlock(String diagramTypeName, String blockName) {
		SubTypeWrapperCreator creator = new SubTypeWrapperCreator(this, diagramTypeName, blockName);
		creator.create();
		return new Pair<>(creator.getNewDiagramType(), creator.getNewWiredBlock());
	}
	
	public class SubTypeWrapperCreator {
		private final DiagramType subType;
		private final String newDiagramTypeName;
		private final String newWiredBlockName;
		private final DiagramType newDiagramType;
		private final WiredBlock newWiredBlock;

		private final Map<FlowDecl, InParameter> toInPar;
		private final Map<FlowDecl, OutParameter> toOutPar;
		private final Set<Node> interceptionSourceNodes;
		
		public SubTypeWrapperCreator(DiagramType subType, String newDiagramTypeName, String newWiredBlockName) {
			this.subType = subType;
			this.newDiagramTypeName = newDiagramTypeName;
			this.newWiredBlockName = newWiredBlockName;
			

			newDiagramType = new DiagramType();
			newDiagramType.setModifiers(new Modifiers());
			newWiredBlock = new WiredBlock();
			newWiredBlock.setModifiers(new Modifiers());
			toInPar = new HashMap<>();
			toOutPar = new HashMap<>();
			
			// Keep track of all local source nodes affected by target interception
			Set<Node> tmpSet = new HashSet<>();
			for (Connection c: subType.connections()) {
				if (!c.isInherited()
						&& isCrossing(c.getSource())
						&& c.declaredFlowDecl() instanceof ConnectionInterception) {
					tmpSet.add(c.getSource().node());
				}
			}
			interceptionSourceNodes = Collections.unmodifiableSet(tmpSet);
		}
		
		public void create() {
			createDiagramTypeContent();
			createWiring();
			createWiredBlock();
		}
		
		public DiagramType getNewDiagramType() {
			return newDiagramType;
		}

		public WiredBlock getNewWiredBlock() {
			return newWiredBlock;
		}
		
		private void createDiagramTypeContent() {
			newDiagramType.setID(newDiagramTypeName);

			Map<Node, InParameter> sourceNodeToPar = new HashMap<>();
			for (Connection c: subType.connections()) {
				if (!c.isInherited()) {
					Expr source;
					VarUse target;
					if (isCrossing(c.getSource())) {
						source = createDiagramTypeInParameter(sourceNodeToPar, c);
					} else {
						source = removeTypedVarUse(c.getSource().treeCopy());
					}
					if (isCrossing(c.getTarget())) {
						target = createDiagramTypeOutParameter(c);
					} else {
						target = removeTypedVarUse(c.getTarget().treeCopy());
					}
					newDiagramType.addFlowDecl(new Connection(source, target));
				}
			}
			for (Block b: subType.getLocalBlocks()) {
				newDiagramType.addLocalBlock(b.treeCopy());
			}
		}

		private void createWiring() {
			Wiring w = new Wiring();
			w.setTypeUse(new TypeUse(newDiagramTypeName));

			// Keep track of handled source nodes. This to handle source nodes with
			// more than one outgoing connection
			Set<Node> sourceNodes = new HashSet<>();
			sourceNodes.addAll(interceptionSourceNodes);
			
			for (Connection c: subType.connections()) {
				if (!c.isInherited()) {
					InParameter inPar = toInPar.get(c.declaredFlowDecl());
					OutParameter outPar = toOutPar.get(c.declaredFlowDecl());
					
					if (isCrossing(c.getSource())) {
						TypeUse type = new TypeUse(c.getSource().type().name());
						if (c.declaredFlowDecl() instanceof SourceInterception) {
							createWiringSourceInterception(w, inPar, outPar, type);
						} else if (c.declaredFlowDecl() instanceof Connection
								&& !sourceNodes.contains(c.getSource().node())) {
							createWiringConnectionSource(w, inPar, type);
							sourceNodes.add(c.getSource().node());
						}
					}
					
					if (isCrossing(c.getTarget())) {
						TypeUse type = new TypeUse(c.getTarget().type().name());
						if (c.declaredFlowDecl() instanceof ConnectionInterception) {
							createWiringTargetInterception(w, inPar, outPar, type);
						} else if (c.declaredFlowDecl() instanceof Connection) {
							createWiringConnectionTarget(w, outPar, type);
						}
					}
				}
			}

			newDiagramType.setLocalWiring(w);
		}

		private void createWiredBlock() {
			newWiredBlock.setType(new TypeUse(newDiagramTypeName));
			newWiredBlock.setName(new IdName(newWiredBlockName));

			// Handle source nodes with several outgoing connections
			Set<Node> sourceNodes = new HashSet<>();
			sourceNodes.addAll(interceptionSourceNodes);
			
			for (Connection c: subType.connections()) {
				if (!c.isInherited()) {
					InParameter inPar = toInPar.get(c.declaredFlowDecl());
					OutParameter outPar = toOutPar.get(c.declaredFlowDecl());

					if (isCrossing(c.getSource())) {
						if (c.declaredFlowDecl() instanceof SourceInterception) {
							Expr source = removeTypedVarUse(c.getSource().treeCopy());
							newWiredBlock.addWiredBlockParameter(
								new WiredBlockParameterVarUse((VarUse) source));
						} else if (c.declaredFlowDecl() instanceof Connection 
								&& !sourceNodes.contains(c.getSource().node())) {
							createWiredParameterForSourceConnection(c, inPar);
							sourceNodes.add(c.getSource().node());
						}
					}
					
					if (isCrossing(c.getTarget())) {
						if (c.declaredFlowDecl() instanceof ConnectionInterception) {
							VarUse target = removeTypedVarUse(c.getTarget().treeCopy());
							newWiredBlock.addWiredBlockParameter(
								new WiredBlockParameterVarUse(target));
						} else if (c.declaredFlowDecl() instanceof Connection) {
							createWiredParameterForTargetConnection(c, outPar);
						}
					}
				}
			}
		}
		
		private boolean isCrossing(Expr e) {
			return e.node().isInherited() || e.node().isParameter();
		}
		
		private <E extends Expr> E removeTypedVarUse(E e) {
			if (e instanceof DotVarUse) {
				DotVarUse dot = (DotVarUse) e;
				if (dot.getVarUse() instanceof TypedSimpleVarUse) {
					TypedSimpleVarUse typed = (TypedSimpleVarUse) dot.getVarUse();
					dot.setVarUse(new SimpleVarUse(typed.getID()));
				} else {
					removeTypedVarUse(((DotVarUse) e).getVarUse());
				}
			}
			return e;
		}

		private String fixName(String name) {
			name = name.replace(".", "");
			if (name.indexOf("::") > 0) {
				name = name.substring(name.indexOf("::")+2);
			}
			return name;
		}

		//
		// Help methods for creating diagram type
		//
		private VarUse createDiagramTypeOutParameter(Connection c) {
			String name = fixName(c.getTarget().toString());
			OutParameter out = new OutParameter(new TypeUse(c.getTarget().type().name()), name);
			newDiagramType.addLocalOutParameter(out);
			toOutPar.put(c.declaredFlowDecl(), out);
			return new SimpleVarUse(name);
		}
		private Expr createDiagramTypeInParameter(Map<Node, InParameter> sourceNodeToPar, Connection c) {
			String name = fixName(c.getSource().toString());
			InParameter in;
			if (!sourceNodeToPar.containsKey(c.getSource().node())) {
				in = new InParameter(new TypeUse(c.getSource().type().name()), name);
				newDiagramType.addLocalInParameter(in);
				sourceNodeToPar.put(c.getSource().node(), in);
			} else {
				in = sourceNodeToPar.get(c.getSource().node());
			}
			toInPar.put(c.declaredFlowDecl(), in);
			return new SimpleVarUse(name);
		}
		
		//
		// Help methods for creating wiring
		//
		private void createWiringTargetInterception(Wiring w, InParameter inPar, OutParameter outPar, TypeUse type) {
			WiringOutParameter wiringOut = new WiringOutParameter(type, outPar.name());
			w.addWiringParameter(wiringOut);
			ConnectionInterception newInterception = new ConnectionInterception(
					new SimpleVarUse(outPar.name()),
					new DotVarUse(new SimpleVarUse(newDiagramTypeName), inPar.name()),
					new DotVarUse(new SimpleVarUse(newDiagramTypeName), outPar.name()));
			w.addFlowDecl(newInterception);
		}
		private void createWiringConnectionTarget(Wiring w, OutParameter outPar, TypeUse type) {
			String name = outPar == null ? "null" : outPar.name();
			WiringOutParameter wiringOut = new WiringOutParameter(type, name);
			w.addWiringParameter(wiringOut);
			Connection newConn = new Connection(
					new DotVarUse(new SimpleVarUse(newDiagramTypeName), name),
					new SimpleVarUse(name));
			w.addFlowDecl(newConn);
		}
		private void createWiringConnectionSource(Wiring w, InParameter inPar, TypeUse type) {
			String name = inPar == null ? "null" : inPar.name();
			WiringInParameter wiringIn = new WiringInParameter(type, name);
			w.addWiringParameter(wiringIn);
			Connection newConn = new Connection(
					new SimpleVarUse(name),
					new DotVarUse(new SimpleVarUse(newDiagramTypeName), name));
			w.addFlowDecl(newConn);
		}
		private void createWiringSourceInterception(Wiring w, InParameter inPar, OutParameter outPar, TypeUse type) {
			WiringInParameter wiringIn = new WiringInParameter(type, inPar.name());
			w.addWiringParameter(wiringIn);
			SourceInterception newInterception = new SourceInterception(
					new SimpleVarUse(inPar.name()),
					new DotVarUse(new SimpleVarUse(newDiagramTypeName), inPar.name()),
					new DotVarUse(new SimpleVarUse(newDiagramTypeName), outPar.name()));
			w.addFlowDecl(newInterception);
		}

		
		//
		// Help methods for creating wired block
		//
		private void createWiredParameterForSourceConnection(Connection c, InParameter inPar) {
			WiredBlockParameter wiredPar;
			if (c.getSource().node().isParameter() && !c.getSource().node().isInherited()) {
				wiredPar = new WiredBlockParameterInParameter(inPar.treeCopy());
			} else {
				VarUse source = (VarUse) removeTypedVarUse(c.getSource().treeCopy());
				wiredPar = new WiredBlockParameterVarUse(source);
			}
			newWiredBlock.addWiredBlockParameter(wiredPar);
		}
		private void createWiredParameterForTargetConnection(Connection c, OutParameter outPar) {
			WiredBlockParameter wiredPar;
			if (c.getTarget().node().isParameter() && !c.getTarget().node().isInherited()) {
				wiredPar = new WiredBlockParameterOutParameter(outPar.treeCopy());
			} else {
				VarUse target = removeTypedVarUse(c.getTarget().treeCopy());
				wiredPar = new WiredBlockParameterVarUse(target);
			}
			newWiredBlock.addWiredBlockParameter(wiredPar);
		}
	}
}